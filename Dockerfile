FROM java:8
VOLUME /tmp
WORKDIR /home
COPY target/booking-rest-0.0.1-SNAPSHOT-spring-boot.jar /app.jar
ADD wait-for-it.sh wait-for-it.sh
RUN bash -c 'chmod +x /wait-for-it.sh'
RUN bash -c 'touch /app.jar'
ENTRYPOINT ["/bin/bash", "/wait-for-it.sh"]
