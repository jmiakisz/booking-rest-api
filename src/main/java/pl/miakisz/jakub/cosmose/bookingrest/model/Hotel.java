package pl.miakisz.jakub.cosmose.bookingrest.model;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="hotel")
public class Hotel {

    @Id
    @Column(name="hotel_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Rating rating;

    @OneToMany(fetch = FetchType.EAGER,orphanRemoval = true)
    @JoinColumn(name="hotel_id")
    private Set<Room> hotelRooms;

    @ManyToOne
    @JsonBackReference
    private Owner owner;


    @NotNull
    private String city;

    public Hotel(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public Set<Room> getHotelRooms() {
        return hotelRooms;
    }

    public void setHotelRooms(Set<Room> hotelRooms) {
        this.hotelRooms = hotelRooms;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
