package pl.miakisz.jakub.cosmose.bookingrest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.miakisz.jakub.cosmose.bookingrest.model.Booking;
import pl.miakisz.jakub.cosmose.bookingrest.model.Room;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {

    public List<Booking> findByRoom(Room room);
}
