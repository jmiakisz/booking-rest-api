package pl.miakisz.jakub.cosmose.bookingrest.model;

public enum Rating {
    POOR,BASIC,COMFORTABLE,SUPERIOR,LUXURY
}
