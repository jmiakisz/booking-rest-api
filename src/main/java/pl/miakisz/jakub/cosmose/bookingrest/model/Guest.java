package pl.miakisz.jakub.cosmose.bookingrest.model;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "guest")
public class Guest extends User{

    @OneToMany(mappedBy="guest")
    private Set<Booking> reservations;

    public Guest() {
    }

    public Set<Booking> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Booking> reservations) {
        this.reservations = reservations;
    }
}
