package pl.miakisz.jakub.cosmose.bookingrest.dto.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.miakisz.jakub.cosmose.bookingrest.dto.HotelDto;
import pl.miakisz.jakub.cosmose.bookingrest.model.Hotel;

import java.util.ArrayList;

@Component
public class HotelDtoConverter implements BaseConverter<Hotel,HotelDto>{

    @Autowired
    RoomDtoConverter roomDtoConverter;

    @Override
    public HotelDto convert(Hotel from) {
        HotelDto hotelDto = new HotelDto();
        hotelDto.setCity(from.getCity());
        hotelDto.setName(from.getName());
        hotelDto.setRating(from.getRating().toString());
        hotelDto.setHotelRooms((ArrayList)roomDtoConverter.convertAll(from.getHotelRooms()));
        return hotelDto;
    }

}
