package pl.miakisz.jakub.cosmose.bookingrest.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class HotelNotFoundException extends RuntimeException{

    public HotelNotFoundException(Long hotelId) {
        super("Hotel with ID '" + hotelId + "' was not found .");
    }
}
