package pl.miakisz.jakub.cosmose.bookingrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.miakisz.jakub.cosmose.bookingrest.model.Owner;

@Repository
public interface OwnerRepository extends JpaRepository<Owner, Long> {
}
