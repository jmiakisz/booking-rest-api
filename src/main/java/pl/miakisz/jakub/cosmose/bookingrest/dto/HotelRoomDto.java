package pl.miakisz.jakub.cosmose.bookingrest.dto;

import org.springframework.hateoas.ResourceSupport;

public class HotelRoomDto extends ResourceSupport {
    private String hotelName;
    private String hotelRating;
    private String city;
}
