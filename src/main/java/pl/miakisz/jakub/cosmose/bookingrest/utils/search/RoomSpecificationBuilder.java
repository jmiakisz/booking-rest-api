package pl.miakisz.jakub.cosmose.bookingrest.utils.search;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import pl.miakisz.jakub.cosmose.bookingrest.model.Room;
import pl.miakisz.jakub.cosmose.bookingrest.model.User;

import java.util.ArrayList;
import java.util.List;

public class RoomSpecificationBuilder {

    private final List<SearchCriteria> params;

    public RoomSpecificationBuilder() {
        params = new ArrayList<SearchCriteria>();
    }

    public RoomSpecificationBuilder with(String key, String operation, Object value) {
        params.add(new SearchCriteria(key, operation, value));
        return this;
    }

    public Specification<Room> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification<Room>> specs = new ArrayList<Specification<Room>>();
        for (SearchCriteria param : params) {
            specs.add(new RoomSpecification(param));
        }

        Specification<Room> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }
}
