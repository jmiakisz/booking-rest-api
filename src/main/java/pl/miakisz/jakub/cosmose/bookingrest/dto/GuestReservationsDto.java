package pl.miakisz.jakub.cosmose.bookingrest.dto;

import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.util.List;

public class GuestReservationsDto extends ResourceSupport{

    private String name;
    private String surname;
    private String username;
    private int reservationsCount;
    private BigDecimal totalReservationsCost;
    private List<ReservationDto> reservations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getReservationsCount() {
        return reservationsCount;
    }

    public void setReservationsCount(int reservationsCount) {
        this.reservationsCount = reservationsCount;
    }

    public BigDecimal getTotalReservationsCost() {
        return totalReservationsCost;
    }

    public void setTotalReservationsCost(BigDecimal totalReservationsCost) {
        this.totalReservationsCost = totalReservationsCost;
    }

    public List<ReservationDto> getReservations() {
        return reservations;
    }

    public void setReservations(List<ReservationDto> reservations) {
        this.reservations = reservations;
    }
}
