package pl.miakisz.jakub.cosmose.bookingrest.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.miakisz.jakub.cosmose.bookingrest.dto.RoomDto;
import pl.miakisz.jakub.cosmose.bookingrest.dto.converter.RoomDtoConverter;
import pl.miakisz.jakub.cosmose.bookingrest.exception.RoomNotFoundException;
import pl.miakisz.jakub.cosmose.bookingrest.model.Period;
import pl.miakisz.jakub.cosmose.bookingrest.model.Room;
import pl.miakisz.jakub.cosmose.bookingrest.service.RoomService;
import pl.miakisz.jakub.cosmose.bookingrest.utils.search.RoomSpecificationBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("room")
public class RoomController {

    @Autowired
    RoomService roomService;

    @Autowired
    RoomDtoConverter roomConverter;


    @ResponseStatus(value= HttpStatus.OK)
    @RequestMapping(value="/{roomId}",method = RequestMethod.PUT)
    public RoomDto editRoom(@PathVariable Long roomId,
                                          @RequestBody RoomDto roomDto){
        this.validateRoom(roomId);
        Room room = roomService.updateRoom(
                roomConverter.convertFromDTO(roomDto));
        roomDto = roomConverter.convert(room);
        roomDto.add(linkTo(RoomController.class).slash(room.getId()).withSelfRel());
        return roomDto;
    }

    @ResponseStatus(value= HttpStatus.OK)
    @RequestMapping(value="/{roomId}",method = RequestMethod.PATCH)
    public RoomDto changeRoomAvailability(@PathVariable Long roomId,
                                          @RequestParam(name="availability") String availability){
        this.validateRoom(roomId);
        Room room = roomService.getRoomById(roomId);
        room.setAvailable(Boolean.valueOf(availability));
        RoomDto roomDto = roomConverter.convert(roomService.updateRoom(room));
        roomDto.add(linkTo(RoomController.class).slash(room.getId()).withSelfRel());
        return roomDto;
    }


    @RequestMapping(method= RequestMethod.GET)
    @ResponseStatus(value=HttpStatus.OK)
    public List<RoomDto> getHotelRooms(@RequestParam(value = "search",required = false) String search,
                                    @RequestParam(value="arrival")@DateTimeFormat(pattern="yyyy-MM-dd")Date dateFrom,
                                    @RequestParam(value="departure")@DateTimeFormat(pattern="yyyy-MM-dd")Date dateTo){
        RoomSpecificationBuilder builder = new RoomSpecificationBuilder();
        if(search!=null){

            Pattern pattern = Pattern.compile("(\\w+?)(:|<|>|/)(\\w+?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }
        }
        Period period = new Period(dateFrom,dateTo);
        builder.with("period",":",period);
        builder.with("available",":",true);
        Specification<Room> spec = builder.build();
        List<Room> rooms=roomService.findAll(spec);
        List<RoomDto> roomDtos = new ArrayList<>();
        for(Room room : rooms){
            RoomDto roomDto = roomConverter.convert(room);
            roomDto.add(linkTo(RoomController.class).slash(room.getId()).withSelfRel());
            roomDtos.add(roomDto);
        }
        return roomDtos;
    }

    private void validateRoom(Long roomId){
        if(this.roomService.getRoomById(roomId) ==null)
            throw new RoomNotFoundException(roomId);
    }
}


