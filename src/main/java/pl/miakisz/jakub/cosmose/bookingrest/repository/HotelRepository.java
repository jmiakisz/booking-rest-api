package pl.miakisz.jakub.cosmose.bookingrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.miakisz.jakub.cosmose.bookingrest.model.Hotel;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long> {
    public Hotel findById(Long hotelId);
}
