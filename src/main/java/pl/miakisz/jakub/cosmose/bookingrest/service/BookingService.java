package pl.miakisz.jakub.cosmose.bookingrest.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.miakisz.jakub.cosmose.bookingrest.exception.RoomNotAvailableException;
import pl.miakisz.jakub.cosmose.bookingrest.model.Booking;
import pl.miakisz.jakub.cosmose.bookingrest.model.Guest;
import pl.miakisz.jakub.cosmose.bookingrest.model.Room;
import pl.miakisz.jakub.cosmose.bookingrest.repository.BookingRepository;
import pl.miakisz.jakub.cosmose.bookingrest.repository.GuestRepository;
import pl.miakisz.jakub.cosmose.bookingrest.repository.RoomRepository;
import pl.miakisz.jakub.cosmose.bookingrest.utils.DateHelper;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Service
public class BookingService {

    @Autowired
    DateHelper dateHelper;

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    GuestRepository guestRepository;


    @Transactional
    public void validateBooking(Long roomId, Date dateFrom, Date dateTo){
        List<Booking> reservationsForRoom = bookingRepository.findByRoom(roomRepository.findById(roomId));
        Room room = roomRepository.findById(roomId);
        for(Booking booking : reservationsForRoom){
            if(dateHelper.dateRangeIntersect(dateFrom,dateTo,booking.getStartDate(),booking.getEndDate())
                    || !room.isAvailable()){
                throw new RoomNotAvailableException(roomId);
            }
        }
    }

    @Transactional
    public Booking createReservation(Long roomId,Long guestId,Date dateFrom,Date dateTo){
        Booking booking = new Booking();
        Room room = roomRepository.findById(roomId);
        Guest guest = guestRepository.findById(guestId);
        booking.setEndDate(dateTo);
        booking.setStartDate(dateFrom);
        booking.setGuest(guest);
        booking.setRooms(room);
       return bookingRepository.save(booking);
    }


}
