package pl.miakisz.jakub.cosmose.bookingrest.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException{

    public BadRequestException() {
        super("Request cannot be processed.");
    }
}
