package pl.miakisz.jakub.cosmose.bookingrest.dto.converter;

import org.springframework.stereotype.Component;
import pl.miakisz.jakub.cosmose.bookingrest.dto.GuestDto;
import pl.miakisz.jakub.cosmose.bookingrest.model.Guest;

@Component
public class GuestDtoConverter implements BaseConverter<Guest,GuestDto>{

    @Override
    public GuestDto convert(Guest from) {
        GuestDto guestDto = new GuestDto();
        guestDto.setLastName(from.getLastName());
        guestDto.setName(from.getName());
        guestDto.setPassword(from.getPassword());
        guestDto.setUsername(from.getPassword());
        return guestDto;
    }

    public Guest convertFromDTO(GuestDto from) {
        Guest guest = new Guest();
        guest.setLastName(from.getLastName());
        guest.setName(from.getName());
        guest.setPassword(from.getPassword());
        guest.setUsername(from.getUsername());
        return guest;
    }
}
