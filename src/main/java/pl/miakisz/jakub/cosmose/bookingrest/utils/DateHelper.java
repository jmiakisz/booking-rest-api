package pl.miakisz.jakub.cosmose.bookingrest.utils;

import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateHelper {

    public int getDaysCountBetweenDates(Date dateFrom,Date dateTo){
        return (int)( (dateTo.getTime() - dateFrom.getTime()) / (1000 * 60 * 60 * 24));
    }

    public boolean dateRangeIntersect(Date start1,Date end1,Date start2,Date end2){
        if ((start1.before(start2) && end1.after(start2)) ||
                (start1.before(end2) && end1.after(end2)) ||
                (start1.before(start2) && end1.after(end2)) ||
                (start1.equals(start2) && end1.equals(end2))){
            return true;
        } else {
            return false;
        }
    }

}
