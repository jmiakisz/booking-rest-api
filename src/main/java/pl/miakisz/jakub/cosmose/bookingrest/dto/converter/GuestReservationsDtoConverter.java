package pl.miakisz.jakub.cosmose.bookingrest.dto.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.miakisz.jakub.cosmose.bookingrest.dto.GuestReservationsDto;
import pl.miakisz.jakub.cosmose.bookingrest.dto.ReservationDto;
import pl.miakisz.jakub.cosmose.bookingrest.model.Guest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Component
public class GuestReservationsDtoConverter implements BaseConverter<Guest,GuestReservationsDto>{

    @Autowired
    ReservationDtoConverter reservationConverter;

    @Override
    public GuestReservationsDto convert(Guest from) {
        GuestReservationsDto guestReservationsDto = new GuestReservationsDto();
        guestReservationsDto.setName(from.getName());
        guestReservationsDto.setSurname(from.getLastName());
        guestReservationsDto.setUsername(from.getUsername());

        if(from.getReservations()!=null && from.getReservations().size() >0){
            guestReservationsDto.setReservationsCount(from.getReservations().size());
            guestReservationsDto.setReservations(from.getReservations()
                    .stream().map(res -> reservationConverter.convert(res))
                    .collect(Collectors.toList()));
            BigDecimal totalCost = new BigDecimal(0);
            for(ReservationDto reservation : guestReservationsDto.getReservations()){
                totalCost = totalCost.add(reservation.getTotalReservationPrice());
            }
            guestReservationsDto.setTotalReservationsCost(totalCost);
        }
        else{
            guestReservationsDto.setReservationsCount(0);
            guestReservationsDto.setTotalReservationsCost(BigDecimal.valueOf(0));
            guestReservationsDto.setReservations(new ArrayList<ReservationDto>());
        }

        return guestReservationsDto;
    }
}
