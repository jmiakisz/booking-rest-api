package pl.miakisz.jakub.cosmose.bookingrest.model;

import java.util.Date;

public class Period {

    Date arrival;
    Date departure;

    public Period(Date arrival, Date departure) {
        this.arrival = arrival;
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }
}
