package pl.miakisz.jakub.cosmose.bookingrest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.miakisz.jakub.cosmose.bookingrest.dto.ReservationDto;
import pl.miakisz.jakub.cosmose.bookingrest.dto.converter.ReservationDtoConverter;
import pl.miakisz.jakub.cosmose.bookingrest.exception.BadRequestException;
import pl.miakisz.jakub.cosmose.bookingrest.exception.UserAlreadyExistsException;
import pl.miakisz.jakub.cosmose.bookingrest.exception.UserNotExistException;
import pl.miakisz.jakub.cosmose.bookingrest.model.Booking;
import pl.miakisz.jakub.cosmose.bookingrest.model.Guest;
import pl.miakisz.jakub.cosmose.bookingrest.service.BookingService;
import pl.miakisz.jakub.cosmose.bookingrest.service.GuestService;
import pl.miakisz.jakub.cosmose.bookingrest.utils.DateHelper;

import java.text.ParseException;
import java.util.Date;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/reservation")
public class BookingController {

    @Autowired
    BookingService bookingService;

    @Autowired
    GuestService guestService;

    @Autowired
    DateHelper dateHelper;

    @Autowired
    ReservationDtoConverter reservationDtoConverter;


    @RequestMapping(method= RequestMethod.POST)
    @ResponseStatus(value= HttpStatus.CREATED)
    public ReservationDto addNewReservation(
            @RequestParam("roomId") Long roomId,
            @RequestParam("dateFrom") @DateTimeFormat(pattern="yyyy-MM-dd")Date dateFrom,
            @RequestParam("dateTo") @DateTimeFormat(pattern="yyyy-MM-dd") Date dateTo,
            @RequestParam("guestId") Long guestId){

        bookingService.validateBooking(roomId,dateFrom,dateTo);
        this.validateGuest(guestId);
        Booking booking = bookingService.createReservation(roomId,guestId,dateFrom,dateTo);
        ReservationDto reservationDto = reservationDtoConverter.convert(booking);
        reservationDto.add(linkTo(BookingController.class).slash(booking.getId()).withSelfRel());
        return reservationDto;
    }

    private void validateGuest(Long guestId) {
        if(this.guestService.getUserById(guestId) ==null)
            throw new UserNotExistException(guestId);
    }
}
