package pl.miakisz.jakub.cosmose.bookingrest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotExistException extends RuntimeException{

    public UserNotExistException(Long userId){
        super("User with ID: " + userId +" not exist.");
    }
}
