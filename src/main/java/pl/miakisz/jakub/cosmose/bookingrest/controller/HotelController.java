package pl.miakisz.jakub.cosmose.bookingrest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.miakisz.jakub.cosmose.bookingrest.dto.HotelDto;
import pl.miakisz.jakub.cosmose.bookingrest.dto.converter.HotelDtoConverter;
import pl.miakisz.jakub.cosmose.bookingrest.model.Hotel;
import pl.miakisz.jakub.cosmose.bookingrest.service.HotelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class HotelController {

    @Autowired
    HotelService hotelService;

    @Autowired
    private HotelDtoConverter hotelDtoConverter;

    @RequestMapping(value = "/hotels", method= RequestMethod.GET)
    @ResponseStatus(value=HttpStatus.OK)
    public ResponseEntity<List<HotelDto>> getHotels() throws Exception {
        List<Hotel> hotels = hotelService.getHotels();
        return new ResponseEntity<List<HotelDto>>((ArrayList)hotelDtoConverter.convertAll(hotels),HttpStatus.OK);
    }


}
