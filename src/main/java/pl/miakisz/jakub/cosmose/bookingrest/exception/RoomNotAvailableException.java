package pl.miakisz.jakub.cosmose.bookingrest.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class RoomNotAvailableException extends RuntimeException{
    public RoomNotAvailableException(Long roomId) {
        super("Room with ID '" + roomId + "' is not available.");
    }
}
