package pl.miakisz.jakub.cosmose.bookingrest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.miakisz.jakub.cosmose.bookingrest.model.Guest;
import pl.miakisz.jakub.cosmose.bookingrest.repository.GuestRepository;

import javax.inject.Inject;
import java.util.stream.Collectors;

@Service
public class GuestService {

    @Autowired
    GuestRepository guestRepository;


    @Transactional
    public Guest register(Guest guest){
       return guestRepository.save(guest);
    }

    @Transactional
    public Guest getUserByUsername(String username){
        return guestRepository.findByUsername(username);
    }

    @Transactional
    public Guest getUserById(Long guestId){
        return guestRepository.findById(guestId);
    }

    @Transactional
    public Guest update(Guest guest){
        return guestRepository.save(guest);
    }

    @Transactional
    public Guest cancelReservation(Guest guest, Long bookingId){
       guest.setReservations(guest.getReservations().stream().filter(res -> !res.getId().equals(bookingId)).collect(Collectors.toSet()));
       return guestRepository.save(guest);
    }
}
