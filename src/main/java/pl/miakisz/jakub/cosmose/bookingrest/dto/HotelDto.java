package pl.miakisz.jakub.cosmose.bookingrest.dto;

import org.springframework.hateoas.ResourceSupport;
import pl.miakisz.jakub.cosmose.bookingrest.model.Hotel;

import java.util.List;

public class HotelDto extends ResourceSupport{

    private String name;
    private String city;
    private String rating;

    private List<RoomDto> hotelRooms;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public List<RoomDto> getHotelRooms() {
        return hotelRooms;
    }

    public void setHotelRooms(List<RoomDto> hotelRooms) {
        this.hotelRooms = hotelRooms;
    }


}
