package pl.miakisz.jakub.cosmose.bookingrest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import pl.miakisz.jakub.cosmose.bookingrest.exception.HotelNotFoundException;
import pl.miakisz.jakub.cosmose.bookingrest.exception.ResourceNotFoundException;

@ControllerAdvice
@EnableWebMvc
public class ExceptionHandlerController {

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseBody
    public ResourceNotFoundException requestHandlingNoHandlerFound() {
        return new ResourceNotFoundException("Resource was not found in path /api/v1");
    }

}
