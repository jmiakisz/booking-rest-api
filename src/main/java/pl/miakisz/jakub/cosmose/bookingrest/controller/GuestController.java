package pl.miakisz.jakub.cosmose.bookingrest.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.miakisz.jakub.cosmose.bookingrest.dto.GuestDto;
import pl.miakisz.jakub.cosmose.bookingrest.dto.GuestReservationsDto;
import pl.miakisz.jakub.cosmose.bookingrest.dto.ReservationDto;
import pl.miakisz.jakub.cosmose.bookingrest.dto.converter.GuestDtoConverter;
import pl.miakisz.jakub.cosmose.bookingrest.dto.converter.GuestReservationsDtoConverter;
import pl.miakisz.jakub.cosmose.bookingrest.exception.UserAlreadyExistsException;
import pl.miakisz.jakub.cosmose.bookingrest.exception.UserNotExistException;
import pl.miakisz.jakub.cosmose.bookingrest.model.Guest;
import pl.miakisz.jakub.cosmose.bookingrest.service.GuestService;

import java.util.Date;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/guest")
public class GuestController {

    @Autowired
    GuestService guestService;

    @Autowired
    GuestDtoConverter guestConverter;

    @Autowired
    GuestReservationsDtoConverter guestReservationsDtoConverter;

    @RequestMapping(method= RequestMethod.POST)
    @ResponseStatus(value= HttpStatus.CREATED)
    public GuestDto register(
            @RequestBody GuestDto guest) {

        this.validateGuest(guest.getUsername());

        Guest registeredGuest = guestService.register(
                guestConverter.convertFromDTO(guest));
        GuestDto guestDto = guestConverter.convert(registeredGuest);
        guestDto.add(linkTo(GuestController.class).slash(registeredGuest.getId()).withSelfRel());
        return guestDto;
    }


    @RequestMapping(value = "/{guestId}/reservations",method=RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public GuestReservationsDto getGuestReservations(
            @PathVariable Long guestId){

       this.validateGuest(guestId);
       Guest guest = guestService.getUserById(guestId);
       GuestReservationsDto guestReservationsDto = guestReservationsDtoConverter.convert(guest);
       guestReservationsDto.add(linkTo(GuestController.class).slash(guest.getId()).withSelfRel());

       return guestReservationsDto;
    }

    @RequestMapping(value = "/{guestId}/reservations/{bookingId}",method=RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public GuestReservationsDto cancelReservation(
            @PathVariable Long guestId,
            @PathVariable Long bookingId){

        this.validateGuest(guestId);
        Guest guest = guestService.getUserById(guestId);
        guest = guestService.cancelReservation(guest,bookingId);
        GuestReservationsDto guestReservationsDto = guestReservationsDtoConverter.convert(guest);
        guestReservationsDto.add(linkTo(GuestController.class).slash(guest.getId()).withSelfRel());

        return guestReservationsDto;
    }


    private void validateGuest(String username) {
        if(this.guestService.getUserByUsername(username) !=null)
            throw new UserAlreadyExistsException(username);
    }
    private void validateGuest(Long guestId) {
        if(this.guestService.getUserById(guestId) ==null)
            throw new UserNotExistException(guestId);
    }
}
