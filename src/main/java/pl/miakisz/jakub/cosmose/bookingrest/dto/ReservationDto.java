package pl.miakisz.jakub.cosmose.bookingrest.dto;

import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReservationDto extends ResourceSupport{

    private static SimpleDateFormat dateFormat= new SimpleDateFormat("MM/dd/yyyy");

    private BigDecimal totalReservationPrice;
    private String dateFrom;
    private String dateTo;
    private RoomDto roomDetails;

    public BigDecimal getTotalReservationPrice() {
        return totalReservationPrice;
    }

    public void setTotalReservationPrice(BigDecimal totalReservationPrice) {
        this.totalReservationPrice = totalReservationPrice;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFormat.format(dateFrom);
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateFormat.format(dateTo);
    }

    public RoomDto getRoomDetails() {
        return roomDetails;
    }

    public void setRoomDetails(RoomDto roomDetails) {
        this.roomDetails = roomDetails;
    }
}
