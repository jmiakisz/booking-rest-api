package pl.miakisz.jakub.cosmose.bookingrest.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "owner")
public class Owner extends User{

    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, mappedBy="owner")
    private Set<Hotel> hotels;

    public Set<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(HashSet<Hotel> hotels) {
        this.hotels = hotels;
    }

    public void setHotels(Set<Hotel> hotels) {
        this.hotels = hotels;
    }
}
