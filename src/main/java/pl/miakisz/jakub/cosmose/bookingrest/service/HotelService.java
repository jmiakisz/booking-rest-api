package pl.miakisz.jakub.cosmose.bookingrest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.miakisz.jakub.cosmose.bookingrest.model.Hotel;
import pl.miakisz.jakub.cosmose.bookingrest.repository.HotelRepository;

import javax.inject.Inject;
import java.util.List;

@Service
public class HotelService {

    @Autowired
    HotelRepository hotelRepository;

    @Transactional
    public List<Hotel> getHotels(){
        return hotelRepository.findAll();
    }

    @Transactional
    public Hotel getHotelById(Long hotelId){
        return hotelRepository.findById(hotelId);
    }
}
