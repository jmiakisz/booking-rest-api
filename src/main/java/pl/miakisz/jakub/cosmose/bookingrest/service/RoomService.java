package pl.miakisz.jakub.cosmose.bookingrest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.miakisz.jakub.cosmose.bookingrest.model.Room;
import pl.miakisz.jakub.cosmose.bookingrest.repository.RoomRepository;

import javax.inject.Inject;
import java.util.List;

@Service
public class RoomService {

    @Autowired
    RoomRepository roomRepository;

    @Transactional
    public Room updateRoom(Room room){
        return roomRepository.save(room);
    }

    @Transactional
    public Room getRoomById(Long roomId){
       return roomRepository.findById(roomId);
    }

    @Transactional
    public List<Room> findAll(Specification<Room> spec){
        return roomRepository.findAll(spec);
    }

    @Transactional
    public List<Room> findAll(){
        return roomRepository.findAll();
    }
}
