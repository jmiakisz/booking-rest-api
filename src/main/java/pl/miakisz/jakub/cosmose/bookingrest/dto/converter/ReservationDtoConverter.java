package pl.miakisz.jakub.cosmose.bookingrest.dto.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.miakisz.jakub.cosmose.bookingrest.dto.ReservationDto;
import pl.miakisz.jakub.cosmose.bookingrest.model.Booking;
import pl.miakisz.jakub.cosmose.bookingrest.utils.DateHelper;

import java.math.BigDecimal;

@Component
public class ReservationDtoConverter implements BaseConverter<Booking,ReservationDto>{

    @Autowired
    DateHelper dateHelper;

    @Autowired
    RoomDtoConverter roomConverter;

    @Override
    public ReservationDto convert(Booking from) {
        int days =dateHelper.getDaysCountBetweenDates(from.getStartDate(),from.getEndDate());
        ReservationDto reservation = new ReservationDto();
        reservation.setTotalReservationPrice(from.getRooms()
                .getPrice().multiply(BigDecimal.valueOf(days)));
        reservation.setRoomDetails(roomConverter.convert(
                from.getRooms()));
        reservation.setDateFrom(from.getStartDate());
        reservation.setDateTo(from.getEndDate());
        return reservation;
    }
}
