package pl.miakisz.jakub.cosmose.bookingrest.dto;

import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;

public class RoomDto extends ResourceSupport {

    private BigDecimal price;
    private int bedCount;
    private int maxGuests;
    private String description;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getBedCount() {
        return bedCount;
    }

    public void setBedCount(int bedCount) {
        this.bedCount = bedCount;
    }

    public int getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(int maxGuests) {
        this.maxGuests = maxGuests;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
