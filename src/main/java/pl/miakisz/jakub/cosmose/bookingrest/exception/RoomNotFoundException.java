package pl.miakisz.jakub.cosmose.bookingrest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RoomNotFoundException extends RuntimeException{

    public RoomNotFoundException(Long roomId) {
        super("Room with ID '" + roomId + "' was not found .");
    }
}
