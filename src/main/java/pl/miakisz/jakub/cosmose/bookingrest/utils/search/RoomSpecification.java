package pl.miakisz.jakub.cosmose.bookingrest.utils.search;

import org.springframework.data.jpa.domain.Specification;
import pl.miakisz.jakub.cosmose.bookingrest.model.Booking;
import pl.miakisz.jakub.cosmose.bookingrest.model.Hotel;
import pl.miakisz.jakub.cosmose.bookingrest.model.Period;
import pl.miakisz.jakub.cosmose.bookingrest.model.Room;

import javax.persistence.criteria.*;

public class RoomSpecification implements Specification<Room> {

    private SearchCriteria criteria;

    public RoomSpecification(SearchCriteria criteria){
        this.criteria =criteria;
    }

    @Override
    public Predicate toPredicate(Root<Room> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        if(criteria.getKey().equalsIgnoreCase("city")){
            Join<Room, Hotel> hotel = root.join("hotel");
            return cb.equal(hotel.get("city"), criteria.getValue().toString());
        }
        else if(criteria.getKey().equalsIgnoreCase("period")){
            Period period = (Period) criteria.getValue();
            Subquery<Room> sq = query.subquery(Room.class);
            Root<Booking> booking = sq.from(Booking.class);
            Join<Booking, Room> sqRooms = booking.join("room");
            sq.select(sqRooms).where(
                    cb.or(
                            cb.and(cb.greaterThanOrEqualTo(booking.get("startDate"),period.getArrival())
                            ,cb.lessThanOrEqualTo(booking.get("endDate"),period.getDeparture())),

                            cb.and(cb.greaterThanOrEqualTo(booking.get("endDate"),
                                    period.getArrival())
                                    ,cb.lessThanOrEqualTo(booking.get("endDate"),period.getDeparture())),

                            cb.and(cb.greaterThanOrEqualTo(booking.get("startDate"),
                                    period.getArrival())
                                    ,cb.lessThanOrEqualTo(booking.get("endDate"),period.getDeparture()))
                    )
            );

            return cb.not(cb.in(root).value(sq));
        }
        else if (criteria.getOperation().equalsIgnoreCase(">")) {
            return cb.greaterThanOrEqualTo(
                    root.<String> get(criteria.getKey()), criteria.getValue().toString());
        }
        else if (criteria.getOperation().equalsIgnoreCase("<")) {
            return cb.lessThanOrEqualTo(
                    root.<String> get(criteria.getKey()), criteria.getValue().toString());
        }
        else if (criteria.getOperation().equalsIgnoreCase(":")) {
            if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return cb.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else {
                return cb.equal(root.get(criteria.getKey()), criteria.getValue());
            }
        }
        return null;
    }


    }