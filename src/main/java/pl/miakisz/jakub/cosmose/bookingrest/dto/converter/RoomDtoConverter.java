package pl.miakisz.jakub.cosmose.bookingrest.dto.converter;


import org.springframework.stereotype.Component;
import pl.miakisz.jakub.cosmose.bookingrest.dto.GuestDto;
import pl.miakisz.jakub.cosmose.bookingrest.dto.RoomDto;
import pl.miakisz.jakub.cosmose.bookingrest.model.Guest;
import pl.miakisz.jakub.cosmose.bookingrest.model.Room;
import pl.miakisz.jakub.cosmose.bookingrest.model.RoomType;

@Component
public class RoomDtoConverter implements BaseConverter<Room,RoomDto>{


    @Override
    public RoomDto convert(Room from) {
        RoomDto roomDto = new RoomDto();
        roomDto.setPrice(from.getPrice());
        roomDto.setBedCount(from.getRoomType().getBedCount());
        roomDto.setDescription(from.getRoomType().getDescription());
        roomDto.setMaxGuests(from.getRoomType().getMaxGuests());
        return roomDto;
    }


    public Room convertFromDTO(RoomDto from) {
        Room room = new Room();
        room.setPrice(from.getPrice());
        RoomType roomType = new RoomType();
        roomType.setBedCount(from.getBedCount());
        roomType.setDescription(from.getDescription());
        roomType.setMaxGuests(from.getMaxGuests());
        return room;
    }
    public Room convertFromDTO(RoomDto from,Room to) {
        to.setPrice(from.getPrice());
        RoomType roomType = to.getRoomType();
        roomType.setBedCount(from.getBedCount());
        roomType.setDescription(from.getDescription());
        roomType.setMaxGuests(from.getMaxGuests());
        return to;
    }


}
