package pl.miakisz.jakub.cosmose.bookingrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.miakisz.jakub.cosmose.bookingrest.model.Guest;

@Repository
public interface GuestRepository extends JpaRepository<Guest, Long> {

   public Guest findById(Long guestId);

   public Guest findByUsername(String username);
}
