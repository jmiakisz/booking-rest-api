package pl.miakisz.jakub.cosmose.bookingrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.miakisz.jakub.cosmose.bookingrest.model.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long>,JpaSpecificationExecutor<Room> {

    public Room findById(Long roomId);
}
