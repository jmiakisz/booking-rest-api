INSERT INTO owner (user_id, name, last_name, username, password)
VALUES
(1000, 'Jakub','Miakisz','jmiakisz', 'pass'),
(2000, 'Kamila','Syga', 'ksyga', 'pass');

INSERT INTO guest (user_id, name, last_name, username, password)
VALUES
(1000, 'Jan','Kazimierz','jkazimierz', 'pass'),
(2000, 'Boleslaw','chrobry', 'bchrobry', 'pass');

INSERT INTO hotel (hotel_id,name, rating, owner_user_id,city)
VALUES
(1000, 'Mariott','SUPERIOR', 1000,'Warszawa'),
(2000, 'Atos','POOR', 1000,'Los Angeles'),
(3000, 'Novotel', 'BASIC', 2000,'Tokio');

INSERT INTO room_type (room_type_id, bed_count, description,max_guests)
VALUES
(1000, 1, 'małżeński apartment',2),
(2000, 3, 'rodzinny apartment',3),
(3000,1, 'biznesowy apartment', 1);

INSERT INTO room (room_id, price, room_number,room_type_room_type_id,available,hotel_hotel_id,hotel_id)
VALUES
(1000, 600,101, 1000,true,1000,1000),
(2000, 80,1099,2000,true,2000,2000),
(3000, 250,12, 3000,true,3000,3000);

INSERT INTO booking (booking_id, end_date, start_date,guest_user_id,room_room_id)
VALUES
(1000, '2017-11-25 00:00:00','2017-11-16 00:00:00',1000,1000),
(2000, '2018-01-01 00:00:00','2017-12-25 00:00:00',1000,2000),
(3000, '2017-12-03 00:00:00','2017-12-01 00:00:00',2000,3000);
